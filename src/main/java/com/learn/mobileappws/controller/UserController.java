package com.learn.mobileappws.controller;

import com.learn.mobileappws.dao.UserDao;
import com.learn.mobileappws.model.request.UserDetailRequestModel;
import com.learn.mobileappws.model.response.UserRest;
import com.learn.mobileappws.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/users")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping
    public String getExample(){
        return"get is called";
    }

    @PostMapping(value ="/updateuser")
    public UserRest createUser(@RequestBody UserDetailRequestModel userDetails){
        UserRest returnValue = new UserRest();
        UserDao userDao = new UserDao();
        BeanUtils.copyProperties(userDetails, userDao);
        UserDao createdUser = userService.createUser(userDao);
        BeanUtils.copyProperties(createdUser, returnValue);
        return returnValue;
    }

}
